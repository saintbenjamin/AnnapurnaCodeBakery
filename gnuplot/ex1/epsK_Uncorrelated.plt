set terminal epslatex size 9cm, 9cm color colortext font 10

set style line 1 lc rgb '#b22222' pt 140 ps 1.5 lt 1 lw 4 # --- red
set style line 2 lc rgb '#5e9c36' pt 139 ps 1.5 lt 1 lw 4 # --- green

set style line 100 lc rgb '#000000' lt 12 lw 3
set style line 101 lc rgb '#87cefa' lt -1 lw 3
set style line 102 lc rgb '#4b0082' lt 1 lw 3

set style arrow 1 head filled size screen 0.025,30,45 ls 1
set style arrow 2 head nofilled size screen 0.03,15 ls 2
set style arrow 3 head filled size screen 0.03,15,45 ls 1
set style arrow 4 head filled size screen 0.03,15 ls 2
set style arrow 5 heads filled size screen 0.03,15,135 ls 1
set style arrow 6 head empty size screen 0.03,15,135 ls 2
set style arrow 7 nohead lc rgb '#000000' lt 1 lw 1.5
set style arrow 8 heads size screen 0.008,90 ls 2

#--------------------------------------------
# Figure for epsK ( Uncorrelated Case Only )
#--------------------------------------------
set output 'epsK_Uncorrelated.tex'
set title '$\vert \epsilon_{K} \vert$ (Uncorrelated Case)'
set size 0.9, 0.9
set origin 0.0, 0.0
set ylabel '$\vert \epsilon_{K} \vert$' rotate by 0
set label '\tiny$(\times 10^{-3})$' at -0.1,2.5 right
set xrange [0:25]
set yrange [1.3:2.5]
set xtics 1,2,24
set key samplen 2.0
c(x)=2.228
u(x)=2.239
l(x)=2.217
plot c(x) with line ls 101 notitle,\
     u(x) with line ls 102 notitle,\
     l(x) with line ls 102 notitle,\
     '< head -24 summary.dat' every 2::0::23 using 5:1:2 with errorbars ls 1 notitle,\
     '< head -24 summary.dat' every 1::0::0 using 5:1:2 ls 1 title '$B_{K}$(LAT.AVG.)',\
     '< head -24 summary.dat' every 2::1::23 using 5:1:2 with errorbars ls 2 notitle,\
     '< head -24 summary.dat' every 1::1::1 using 5:1:2 ls 2 title '$B_{K}$(SWME)'
