set terminal epslatex size 12cm, 9cm color colortext font 9

# black / continuous line / circle with central dot
set style line 1 pt 6 ps 1.1 lt 1 lw 1.2 lc rgb '#000000'
# red
set style line 11 pt 140 ps 1.5 lt 1 lw 4 lc rgb '#b22222'
# green
set style line 21 pt 139 ps 1.5 lt 1 lw 4 lc rgb '#5e9c36'  

# black / continuous line 
set style line 101 lt 1 lw 3 lc rgb '#000000' 
# crimson /
set style line 102 lt 1 lw 1.5 lc rgb '#dc143c'
#
set style line 103 lt 1 lw 2 lc rgb '#87cefa'
# darkorange / dotted line
set style line 104 lt 13 lw 1.2 lc rgb '#ff8c00'
#
set style line 105 lt 13 lw 1.2 lc rgb '#4b0082'
# darkcyan /
set style line 106 lt 13 lw 1.2 lc rgb '#008b8b'
# crimson /
set style line 107 lt 13 lw 1.2 lc rgb '#dc143c'

set style arrow 1 head filled size screen 0.025,30,45 ls 1
set style arrow 2 head nofilled size screen 0.03,15 ls 2
set style arrow 3 head filled size screen 0.03,15,45 ls 1
set style arrow 4 head filled size screen 0.03,15 ls 2
set style arrow 5 heads filled size screen 0.03,15,135 ls 1
set style arrow 6 head empty size screen 0.03,15,135 ls 2
set style arrow 7 nohead lc rgb '#000000' lt 1 lw 1.5
set style arrow 8 heads size screen 0.008,90 ls 2

#--------------------------------------
# Effective mass plot with fit results
#--------------------------------------
set output 'meff.tex'
set title 'pi\_d\_d\_m0.05\_k0.038\_p000'
set size 0.9, 0.9
set origin 0.0, 0.0
#set ylabel '$m_\text{eff}$' rotate by 90
set label '$\chi^2/\text{d.o.f} = 1.453(890)$' at 61.0,2.10 right
set label '$c_1 = 0.100(7)$' at 2.0,2.20 left
set label '$c_2 = 2.300(5)$' at 2.0,2.15 left
set label '$c_3 = 0.096(247)$' at 2.0,2.10 left
set label '$c_4 = 9.274(55.9)$' at 2.0,2.05 left
set xrange [0:63]
set yrange [2:3]
#set xtics 1,2,24
#set xtics 1,,62
set ytics 0.1
#set key samplen 2.0
c(x)=2.30010
u(x)=2.30469
l(x)=2.29551
plot u(x) with line ls 107 notitle,\
     l(x) with line ls 107 notitle,\
     c(x) with line ls 102 title '$c_2$',\
     '< head -64 pi_d_d_m0.05_k0.038_p000_meff.dat' using 1:2:3 with errorbars ls 1 title '$m_\text{eff}$'
