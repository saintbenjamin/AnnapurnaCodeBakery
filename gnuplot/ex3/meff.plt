#!/bin/sh

FILE=$1
HDR="/tmp/plot_header.tmp"
DAT="/tmp/plot_data.tmp"

# separate a header and data field
awk '/#BEGIN HEADER/,/#END HEADER/' < ${FILE}\
| grep -v HEADER > ${HDR}

grep -v ^# ${FILE} > ${DAT}


# parse the header
CHANNEL=$(awk '$1 ~ /#channel/ {for(i=2;i<=NF;i++) print $i}'\
          < ${HDR}\
		  | sed 's/_/\\_/g')

FTYPE=$(awk '$1 ~ /#fit_type/ {print $2}'< ${HDR})

FR=($(awk '$1 ~ /#fit_range/ {print $2, $3}'< ${HDR}))

CDF=($(awk '$1 ~ /#cdf/ {print $2, $3}'< ${HDR}))

Q=($(awk '$1 ~ /#quality/ {print $2, $3}'< ${HDR}))

MIDX=$(awk '$1 ~ /#i_meff/ {print $2}'< ${HDR})
let "MIDX--"

CFIT=($(awk '$1 ~ /#c_fit/ {for(i=2;i<=NF;i++) printf "%.3f ", $i }'\
        < ${HDR}))

CERR=($(awk '$1 ~ /#c_err/ {for(i=2;i<=NF;i++) printf "%.3f ", $i }'\
        < ${HDR}))


# determine a plot range and lable positions
X=($(awk 'BEGIN{min=1e30;max=1e-30}
	           {if($1<min) min=$1; if($1>max) max=$1}
			END{printf "%f %f", min, max}' < ${DAT}))

Y=($(awk 'BEGIN{min=1e30;max=1e-30}
	           {if($2<min) min=$2; if($2>max) max=$2}
			END{printf "%f %f", min, max}' < ${DAT}))


# draw a effective mass plot
gnuplot << EOF
set terminal epslatex size 12cm, 9cm color colortext font 9

# black / continuous line / circle with central dot
set style line 1 pt 6 ps 1.1 lt 1 lw 1.2 lc rgb '#000000'

# crimson / continuous line
set style line 102 lt 1 lw 1.5 lc rgb '#dc143c'

# crimson / dotted line
set style line 107 lt 13 lw 1.5 lc rgb '#dc143c'

set output 'meff.tex'
set title '${CHANNEL}'
set size 0.9, 0.9
set origin 0.0, 0.0
set xrange [${X[0]}:${X[1]}]
set yrange [2:ceil(10*${Y[1]})*0.1]
#set xtics 1,2,24
#set xtics 1,,62
set ytics 0.1
#set key samplen 2.0
#set ylabel '\$m_\text{eff}\$' rotate by 90
set label '\$\chi^2/\text{d.o.f} = ${CDF[0]}(${CDF[1]})\$'\
          at 61.0,2.10 right
set label '\$c_1 = ${CFIT[0]}(${CERR[0]})\$' at 2.0,2.20 left
set label '\$c_2 = ${CFIT[1]}(${CERR[1]})\$' at 2.0,2.15 left
set label '\$c_3 = ${CFIT[2]}(${CERR[2]})\$' at 2.0,2.10 left
set label '\$c_4 = ${CFIT[3]}(${CERR[3]})\$' at 2.0,2.05 left
m(x) = (x>=${FR[0]}&& x<=${FR[1]}) ? ${CFIT[$MIDX]} : 1/0
e(x) = (x>=${FR[0]} && x<${FR[1]}) ? ${CERR[$MIDX]} : 1/0
plot m(x)+e(x) with line ls 107 notitle,\
     m(x)-e(x) with line ls 107 notitle,\
     m(x) with line ls 102 title '\$c_2\$',\
	 '${DAT}' using 1:2:3 with errorbars ls 1 title '\$m_\text{eff}\$'
EOF
