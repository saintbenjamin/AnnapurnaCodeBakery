#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(void)
{
  int i;
  
  gsl_rng *rng;
  double u;
  double g, sigma=10.0;
  unsigned long int seed = 1234;
  int num = 20;

  rng = gsl_rng_alloc(gsl_rng_mt19937);  // select generator
  gsl_rng_set(rng,seed);				 // seed the generator

  printf("generate uniform random numbers\n");
  for( i = 1; i <= num; i++ ){
	u = gsl_rng_uniform(rng);   
	printf("%d\t%lf\n",i,u);
  }

  putc('\n',stdout);

  printf("generate gaussian distributed random numbers\n");
  for( i = 1; i <= num; i++ ){
	g = gsl_ran_gaussian(rng,sigma);  
	printf("%d\t%lf\n",i,g);
  }

  gsl_rng_free(rng);

  return 0;
}
