// template specialization

#include <iostream>
using namespace std;

// class template:
template <class T>
class mycontainer {
  T element;
  public:
    mycontainer ( T );
    T increase ();
};

template <class T>
mycontainer< T >::mycontainer(T arg){
  element=arg;
}

template <class T>
T mycontainer< T >::increase(){
  return ++element;
}
