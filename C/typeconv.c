#include <stdio.h>

//-------------------------------------------------------------------------
// testing implicite typeconversion
//-------------------------------------------------------------------------
// Argument _c_ in the function _foo_ is treated as the type _int_
// , gcc with -std=c99 option.
//-------------------------------------------------------------------------
void foo(c){
  
  printf("char variable = %d\n", c);

}

int typeconv(void){
  
  float c = 10.1;
  
  foo(c);

  return 0;

}
