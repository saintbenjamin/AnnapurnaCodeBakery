#include <stdio.h>
#include <math.h>

//-------------------------------------------------------------------------
// testing the exponent with negative base
//-------------------------------------------------------------------------
// NOTE:
//-------------------------------------------------------------------------
int exponent(void){
  
  float a, b;
  int n;
  
  for( n = 0; n < 10; n++ ){
    b = pow(-1.0,n);
    printf("-1.0 to the %d is equal to %f\n",n,b);
  }

  b=pow(-1.0,0.3);
  printf("-1.0 to the 0.3 is equal to %f\n",b);

  return 0;
}
