#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include "module-gsl/matrix.h"

#define MAT_DIM 8

//-------------------------------------------------------------------------
// testing a square matrix inversion using Faddev-Leverrier algorithm
//-------------------------------------------------------------------------
// NOTE:
//-------------------------------------------------------------------------

int matInv_FaddevLeverrier(void){
 
  int n = MAT_DIM;
  double max_d;
  double tmp;
  double p[MAT_DIM];

  const int scale_diagonal = 0;
  double weight = 1.4;

  const int symmetric = 1;
  const int normalize = 1;
  // test matrix 1
  //double A[9] = { 21.14, -2.58, 4.22, 9.91, 46.34, 2.35, -17.83, -8.18, 9.73};
  // test matrix 2
  double A[64] = {
           4.082643e-06, 4.320807e-06, 4.568584e-06, 4.826229e-06, 5.144017e-06, 5.425515e-06, 5.717707e-06, 6.395958e-06,
           0.000000e+00, 4.593945e-06, 4.879303e-06, 5.177055e-06, 5.545234e-06, 5.873117e-06, 6.214262e-06, 7.009295e-06,
           0.000000e+00, 0.000000e+00, 5.205919e-06, 5.548161e-06, 5.970695e-06, 6.351427e-06, 6.749041e-06, 7.678282e-06,
           0.000000e+00, 0.000000e+00, 0.000000e+00, 5.939599e-06, 6.419846e-06, 6.860169e-06, 7.322406e-06, 8.404272e-06,
           0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 6.993140e-06, 7.500951e-06, 8.034209e-06, 9.308018e-06,
           0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 8.084628e-06, 8.701220e-06, 1.017386e-05,
           0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 9.411778e-06, 1.110514e-05,
           0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 1.335556e-05 };
  // test matrix 3
  //double A[64] = {
  //        0, 1, 0, 2, 0, 0, 0, 1,
  //        1, 0, 0, 0, 0, 0, 0, 2,
  //        0, 0, 1, 0, 1, 0, 0, 0,
  //        2, 0, 0, 3, 0, 0, 0, 0,
  //        0, 0, 1, 0, 8, 0, 0, 0,
  //        0, 0, 0, 0, 0, 1, 0, 0,
  //        2, 0, 0, 0, 0, 0, 1, 0,
  //        1, 0, 0, 0, 0, 0, 0, 0 };
  // test matrix 4
  //double A[9] = {
  //        0, 0, 1,
  //        0, 1, 0,
  //        1, 0, 0 };
  // test matrix 5
  //double A[64] = {
  //         4.082643e-05, 4.320807e-06, 4.568584e-06, 4.826229e-06, 5.144017e-06, 5.425515e-06, 5.717707e-06, 6.395958e-06,
  //         0.000000e+00, 4.593945e-05, 4.879303e-06, 5.177055e-06, 5.545234e-06, 5.873117e-06, 6.214262e-06, 7.009295e-06,
  //         0.000000e+00, 0.000000e+00, 5.205919e-05, 5.548161e-06, 5.970695e-06, 6.351427e-06, 6.749041e-06, 7.678282e-06,
  //         0.000000e+00, 0.000000e+00, 0.000000e+00, 5.939599e-05, 6.419846e-06, 6.860169e-06, 7.322406e-06, 8.404272e-06,
  //         0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 6.993140e-05, 7.500951e-06, 8.034209e-06, 9.308018e-06,
  //         0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 8.084628e-05, 8.701220e-06, 1.017386e-05,
  //         0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 9.411778e-05, 1.110514e-05,
  //         0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 0.000000e+00, 1.335556e-04 };
  // test matrix 6
  //double A[64] = {
  //        1,  2,  3,  4,  5,  6,  7,  8,
  //        0,  9, 10, 11, 12, 13, 14, 15,
  //        0,  0, 16, 17, 18, 19, 20, 21,
  //        0,  0,  0, 22, 23, 24, 25, 26,
  //        0,  0,  0,  0, 27, 28, 29, 30,
  //        0,  0,  0,  0,  0, 31, 32, 33,
  //        0,  0,  0,  0,  0,  0, 34, 35,
  //        0,  0,  0,  0,  0,  0,  0, 36 };
  // test matrix 7
  //double A[16] = {
  //        5,  6,  7,  8,
  //        0,  9, 11, 12, 
  //        0,  0, 18, 20,
  //        0,  0,  0, 26 };
  // test matrix 8
  //double A[25] = {
  //        2,  3,  5,  6,  8,
  //        0,  9, 10, 11, 12,
  //        0,  0, 16, 19, 21,
  //        0,  0,  0, 22, 24,
  //        0,  0,  0,  0, 30 };

  gsl_matrix *matA = gsl_matrix_calloc(n,n);
  gsl_matrix *matAinv = gsl_matrix_calloc(n,n);
  gsl_matrix *matT = gsl_matrix_calloc(n,n);

  // initialize, A = T

  max_d = 0.0;
  
  if (scale_diagonal == 1) {
    for ( int i = 0; i < n; i++ ) {
      if ( max_d < A[i*n+i] ) { max_d = A[i*n+i]; }
      gsl_matrix_set(matA,i,i,A[i*n+i]*weight);
    }
  }else{
    for ( int i = 0; i < n; i++ ) {
      if ( max_d < A[i*n+i] ) { max_d = A[i*n+i]; }
      gsl_matrix_set(matA,i,i,A[i*n+i]);
    }
  }
      
  if (symmetric == 1) {
    for ( int i = 0; i < n; i++ ) {
      for ( int j = i+1; j < n; j++ ) {
        if ( max_d < A[i*n+j] ) { max_d = A[i*n+j]; }
        //gsl_matrix_set(matA,i,j,A[i*n+j]/sqrt(A[i*n+i]*A[j*n+j]));
        //gsl_matrix_set(matA,j,i,A[i*n+j]/sqrt(A[i*n+i]*A[j*n+j]));
        gsl_matrix_set(matA,i,j,A[i*n+j]);
        gsl_matrix_set(matA,j,i,A[i*n+j]);
      }
    }
  }else{
    for ( int i = 0; i < n; i++ ) {
      for ( int j = i+1; j < n; j++ ) {
        if ( max_d < A[i*n+j] ) { max_d = A[i*n+j]; }
        if ( max_d < A[j*n+i] ) { max_d = A[j*n+i]; }
        gsl_matrix_set(matA,i,j,A[i*n+j]);
        gsl_matrix_set(matA,j,i,A[j*n+i]);
      }
    }
  }
 
  if (scale_diagonal == 1) { max_d *= weight; }

  if (normalize == 1) { gsl_matrix_scale(matA,1.0/max_d); }
  
  gsl_matrix_memcpy(matT,matA);
  
  //printf("matrix A = \n");
  //printMat(matA,n);
  //
  //printf("matrix T = \n");
  //printMat(matT,n);

  // begin Faddev-Leverrier
  p[0] = traceMat(matT,n);

  for( int k = 1; k < n-1; k++){
    addMatConstMultId(matT,-p[k-1],n);
    multMat(matA,matT,n);
    p[k] = traceMat(matT,n)/(double (k+1));
  }
  
  addMatConstMultId(matT,-p[n-2],n);
  gsl_matrix_memcpy(matAinv,matT);
  multMat(matA,matT,n);
  p[n-1] = traceMat(matT,n)/(double (n));
 
  gsl_matrix_scale(matAinv,1.0/p[n-1]);
  gsl_matrix_scale(matT,1.0/p[n-1]); 
  // end Faddev-Leverrier
  
  printf("matrix A = \n");
  printMat(matA,n);
 
  printf("matrix Ainv = \n");
  printMat(matAinv,n);
 
  //printf("matrix T ?= Id(n),\n");
  //printMat(matT,n);
  
  multMat(matA,matAinv,n);
  printf("matrix A*Ainv ?= Id(n),\n");
  printMat(matAinv,n);
  
  //multMat(matA,matT,n);
  //printf("matrix A*T ?= A,\n");
  //printMat(matT,n);
  
  multMat(matA,matAinv,n);
  printf("matrix A*A*Ainv ?= A,\n");
  printMat(matAinv,n);
  
  for( int i = 0; i < n; i++ ){
    printf("p[ %3d ] = %13.6le\n",i,p[i]);
  }

  // compare with SVD
  gsl_matrix *matU = gsl_matrix_calloc(n,n);
  gsl_vector *vecS = gsl_vector_calloc(n);
  gsl_matrix *matV = gsl_matrix_calloc(n,n);
  gsl_vector *wsv = gsl_vector_calloc(n);

  gsl_matrix_memcpy(matU,matA);
  gsl_linalg_SV_decomp(matU,matV,vecS,wsv);
  
  for( int i = 0; i < n; i++ ) {
    for( int j = 0; j < n; j++ ) {
      tmp = 0.0;
      for( int k = 0; k < n; k++ ) {
        tmp += gsl_matrix_get(matV,i,k)*(1.0/gsl_vector_get(vecS,k))*gsl_matrix_get(matU,j,k);
      }
      gsl_matrix_set(matAinv,i,j,tmp);
    }
  }

  printf("\n==== Inverse using SVD ====\n");
  
  printf("matrix A = \n");
  printMat(matA,n);

  printf("matrix Ainv = \n");
  printMat(matAinv,n);
 
  multMat(matA,matAinv,n);
  printf("matrix A*Ainv ?= Id(n),\n");
  printMat(matAinv,n);
  
  multMat(matA,matAinv,n);
  printf("matrix A*A*Ainv ?= A,\n");
  printMat(matAinv,n);
  
  for( int i = 0; i < n; i++ ){
    printf("s[ %3d ] = %13.6le\n",i,gsl_vector_get(vecS,i));
  }
  
  // compare with SVD using Jacobi method
  // It is expected that the singular values are obtained in higher
  // relative accuracy than the default SVD routine gsl_linalg_SV_decomp().
  gsl_matrix_memcpy(matU,matA);
  gsl_linalg_SV_decomp_jacobi(matU,matV,vecS);
  
  for( int i = 0; i < n; i++ ) {
    for( int j = 0; j < n; j++ ) {
      tmp = 0.0;
      for( int k = 0; k < n; k++ ) {
        tmp += gsl_matrix_get(matV,i,k)*(1.0/gsl_vector_get(vecS,k))*gsl_matrix_get(matU,j,k);
      }
      gsl_matrix_set(matAinv,i,j,tmp);
    }
  }

  printf("\n==== Inverse using SVD (Jacobi) ====\n");
  
  printf("matrix A = \n");
  printMat(matA,n);

  printf("matrix Ainv = \n");
  printMat(matAinv,n);
 
  multMat(matA,matAinv,n);
  printf("matrix A*Ainv ?= Id(n),\n");
  printMat(matAinv,n);
  
  multMat(matA,matAinv,n);
  printf("matrix A*A*Ainv ?= A,\n");
  printMat(matAinv,n);
  
  for( int i = 0; i < n; i++ ){
    printf("s[ %3d ] = %13.6le\n",i,gsl_vector_get(vecS,i));
  }
  

  gsl_matrix_free(matA);
  gsl_matrix_free(matAinv);
  gsl_matrix_free(matT);

  gsl_matrix_free(matU);
  gsl_vector_free(vecS);
  gsl_matrix_free(matV);
  gsl_vector_free(wsv);
  
  return 0;
}
