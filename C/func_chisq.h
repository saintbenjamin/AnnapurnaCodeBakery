#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#ifndef __FUNC_CHISQ_H__
#define __FUNC_CHISQ_H__

struct FitData{
  int n1;
  int n2;
  int nt;
  gsl_vector *avg;
  gsl_matrix *invcov;
  int nparam;
  gsl_vector *p;
  gsl_vector *iw;
};

double chiSq(const gsl_vector *params, void *fit);
void dchiSq(const gsl_vector *params, void *fit, gsl_vector *dchisq);
void chiSq_dchiSq(const gsl_vector *params, void *fit, double *chisq, gsl_vector *dchisq);

#endif
