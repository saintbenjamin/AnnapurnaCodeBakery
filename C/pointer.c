#include <stdio.h>
#include <stdlib.h>
#include "type.h"

#define N 3
#define M 5
#define X 10

int printValue(int *data[][M]);
int printValue2(int (*ptr)[M]);
int printValue3(int *ptr);
int printValue4(mycomplex *ptr[][M]);
int printValue5(double *data[][M]);
int printValue6(double *ptr[]);

int pointer(void){

  const int nrow = N;
  const int mcol = M;
  const int xsite = X;
  int i,j,k;
  int rval;

  int *ptr[nrow][mcol];
  int a2d[nrow][mcol];
  int a3d[xsite][nrow][mcol];

  mycomplex *z[nrow][mcol];

  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      ptr[i][j] = (int *) malloc(sizeof(int)*xsite);
      z[i][j] = (mycomplex *) malloc(sizeof(mycomplex)*xsite);
    }
  }
  
  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      for(k=0; k<xsite; k++){
        rval = (i-j+15)*(k+1);
        *(ptr[i][j]+k) = rval;
        z[i][j][k].re = rval;
        z[i][j][k].im = -rval;
        a3d[k][i][j] = rval;
      }
      a2d[i][j] = rval;
    }
  }
/*
  printf("Test 3d. array constructed by 2d. pointer array\n");
  printValue((void *)ptr);

  printf("Test 2d. array passed to the (pointer) 1d. array. \n");
  printValue2(a2d);
  
  printf("Test 2d. array passed to the pointer. \n");
  printValue3(a2d[2]);
  
  printf("Test 3d. array passed to the (pointer) 1d. array. \n");
  printValue2(a3d[9]);
  
  printf("Test 3d. complex structure array passed to the 2d. pointer array. \n");
  printValue4(z);
*/  
  printf("Test6\n");
  printValue4(z);
  printValue5((void *)z);
  printValue4(z);
/*  
  printf("Test7\n");
  printValue6((void *)z[2]);
*/
  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      free(ptr[i][j]);
      free(z[i][j]);
    }
  }
  
  return 0;
}

int printValue(int *ptr[][M]){

  const int nrow = N;
  const int mcol = M;
  const int xsite = X;
  int i,j,k;

  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      for(k=0; k<xsite; k++){
        printf( "%d, ", *(ptr[i][j]+k) );
      }
      printf("\n");
    }
  }

  return 0;
}

int printValue2(int (*ptr)[M]){

  const int nrow = N;
  const int mcol = M;
  int i,j;

  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      printf( "%d, ", ptr[i][j] );
    }
    printf("\n");
  }

  return 0;
}

int printValue3(int *ptr){

  const int mcol = M;
  int i;

  for(i=0; i<mcol; i++){
    printf( "%d, ", ptr[i] );
    printf("\n");
  }

  return 0;
}

int printValue4(mycomplex *ptr[][M]){

  const int nrow = N;
  const int mcol = M;
  const int xsite = X;
  int i,j,k;

  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
      for(k=0; k<xsite; k++){
        //printf( "(%4.2lf, %4.2lf), ", ptr[i][j][k].re, ptr[i][j][k].im );
        printf( "(%4.2lf, %4.2lf), ", (ptr[i][j]+k)->re, (ptr[i][j]+k)->im );
      }
      printf("\n");
    }
  }

  return 0;
}

int printValue5(double *ptr[][M]){

  const int nrow = N;
  const int mcol = M;
  const int xsite = X;
  int i,j,k;

  double *ptr2[3];

  for(i=0; i<nrow; i++){
    for(j=0; j<mcol; j++){
     ptr2[2] = (ptr[i][j]);
      for(k=0; k<xsite; k++){
        printf( "%e, ", *ptr2[2] );
        ptr2[2] += 1;
      }
      printf("\n");
    }
  }

  return 0;
}

int printValue6(double *ptr[]){

  const int mcol = M;
  const int xsite = X;
  int j,k;

  for(j=0; j<mcol; j++){
    for(k=0; k<xsite; k++){
      printf( "%e, ", *(ptr[j]+k) );
    }
    printf("\n");
  }

  return 0;
}
#undef N
#undef M
#undef X
