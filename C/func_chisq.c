#include <stdio.h>
#include <cmath>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "func_chisq.h"

double fitFunc(const double *c, int i, const int nt, int npa) {

    double tt, tsym, c2, c6, c10;

    tt = - ((double)i);
    tsym = (double)(i-nt);


    double y;
   
    if (npa == 4) {
      
      c2 = (1.0-cosh(c[2]));
   
      if( i%2 != 0 ){
        c2 = -c2;
      }
      
      y = c[0] * ( 
            exp(tt * c[1]) * ( 1.0  + c2 * exp(tt * c[3]) )
            + exp(tsym * c[1]) * ( 1.0 + c2 * exp(tsym * c[3]) )
            );
    }
    else if (npa == 8) {
      
      c2 = (1.0-cosh(c[2]));
      c6 = (1.0-cosh(c[6]));
   
      if( i%2 != 0 ){
        c2 = -c2;
        c6 = -c6;
      }
      
      y = c[0] * ( 
            exp(tt * c[1]) * ( 
              1.0  + c2 * exp(tt * c[3]) 
              + c[4] * exp(tt * c[5]) * ( 1.0 + c6 * exp(tt * c[7]) )
              )
            + exp(tsym * c[1]) * ( 
              1.0 + c2 * exp(tsym * c[3]) 
              + c[4] * exp(tsym * c[5]) * ( 1.0 + c6 * exp(tsym * c[7]) ) 
              )
            );
    }
    else if (npa == 12) {
      
      c2 = (1.0-cosh(c[2]));
      c6 = (1.0-cosh(c[6]));
      c10 = (1.0-cosh(c[10]));
   
      if( i%2 != 0 ){
        c2 = -c2;
        c6 = -c6;
        c10 = -c10;
      }
      
      y = c[0] * ( 
            exp(tt * c[1]) * ( 
              1.0  + c2 * exp(tt * c[3]) 
              + c[4] * exp(tt * c[5]) * ( 
                1.0 + c6 * exp(tt * c[7]) 
                + c[8] * exp(tt * c[9]) * ( 
                  1.0 + c10 * exp(tt * c[11]) 
                  )
                )
              )
            + exp(tsym * c[1]) * ( 
              1.0 + c2 * exp(tsym * c[3]) 
              + c[4] * exp(tsym * c[5]) * ( 
                1.0 + c6 * exp(tsym * c[7]) 
                + c[8] * exp(tsym * c[9]) * ( 
                  1.0 + c10 * exp(tsym * c[11]) 
                  ) 
                ) 
              )
            );
    }
    else{
      exit(100);
    }
    
    return y;
}


double da_dfitFunc(const double *c, int a, int i, const int nt, int npa) {

    double tt, tsym, c2, c6, c10, dc2, dc6, dc10, df;

    tt = - ((double)i);
    tsym = (double)(i-nt);
    
    if (npa == 4) {
    
      c2 = (1.0-cosh(c[2]));
      dc2 = - sinh(c[2]);
      
      if( i%2 != 0 ){
        c2 = -c2;
        dc2 = -dc2;
      }
      
      switch(a){
        case 0:
          df = exp(tt * c[1]) * ( 1.0 + c2 * exp(tt * c[3]) )
               + exp(tsym * c[1]) * ( 1.0 + c2 * exp(tsym * c[3]) );
          break;

        case 1:
          df = c[0] * ( 
                tt * exp(tt * c[1]) * ( 1.0 + c2 * exp(tt * c[3]) )
                + tsym * exp(tsym * c[1]) * ( 1.0 + c2 * exp(tsym * c[3]) )
                );
          break;

        case 2:
          df = c[0] * dc2 * 
              ( exp(tt * (c[1]+c[3])) + exp(tsym * (c[1]+c[3])) );
          break;

        case 3:
          df = c[0] * c2 *
              ( exp(tt * (c[1]+c[3])) * tt + exp(tsym * (c[1]+c[3])) * tsym );
          break;
      }
    }
    else if (npa == 8) {
    
      c2 = (1.0-cosh(c[2]));
      c6 = (1.0-cosh(c[6]));
      dc2 = - sinh(c[2]);
      dc6 = - sinh(c[6]);
      
      if( i%2 != 0 ){
        c2 = -c2;
        c6 = -c6;
        dc2 = -dc2;
        dc6 = -dc6;
      }
      
      switch(a){
        case 0:
          df = exp(tt * c[1]) * ( 
                 1.0 + c2 * exp(tt * c[3]) 
                 + c[4] * exp(tt * c[5]) * ( 1.0 + c6 * exp(tt * c[7]) )
                 )
               + exp(tsym * c[1]) * ( 
                 1.0 + c2 * exp(tsym * c[3]) 
                 + c[4] * exp(tsym * c[5]) * ( 1.0 + c6 * exp(tsym * c[7]) ) 
                 );
          break;

        case 1:
          df = c[0] * ( 
                tt * exp(tt * c[1]) * ( 
                  1.0 + c2 * exp(tt * c[3]) 
                  + c[4] * exp(tt * c[5]) * ( 1.0 + c6 * exp(tt * c[7]) )
                  )
                + tsym * exp(tsym * c[1]) * ( 
                  1.0 + c2 * exp(tsym * c[3]) 
                  + c[4] * exp(tsym * c[5]) * ( 1.0 + c6 * exp(tsym * c[7]) ) 
                  )
                );
          break;

        case 2:
          df = c[0] * dc2 * 
              ( exp(tt * (c[1]+c[3])) + exp(tsym * (c[1]+c[3])) );
          break;

        case 3:
          df = c[0] * c2 *
              ( exp(tt * (c[1]+c[3])) * tt + exp(tsym * (c[1]+c[3])) * tsym );
          break;
        
        case 4:
          df = c[0] * ( 
                 exp(tt * (c[1]+c[5])) * ( 1.0 + c6 * exp(tt * c[7]) )
                 + exp(tsym * (c[1]+c[5])) * ( 1.0 + c6 * exp(tsym * c[7]) ) 
               );
          break;

        case 5:
          df = c[0] * c[4] * ( 
                 exp(tt * (c[1]+c[5])) * tt * (1.0 + c6 * exp(tt * c[7]))
                 + exp(tsym * (c[1]+c[5])) * tsym * (1.0 + c6 * exp(tsym * c[7])) 
              );
          break;

        case 6:
          df = c[0] * c[4] * dc6 *
              ( exp(tt * (c[1]+c[5]+c[7])) + exp(tsym * (c[1]+c[5]+c[7])) );
          break;

        case 7:
          df = c[0] * c[4] * c6 *
              ( exp(tt * (c[1]+c[5]+c[7])) * tt + exp(tsym * (c[1]+c[5]+c[7]) ) * tsym );
          break;
      }
    }
    else if (npa == 12) {
    
      c2 = (1.0-cosh(c[2]));
      c6 = (1.0-cosh(c[6]));
      c10 = (1.0-cosh(c[10]));
      dc2 = - sinh(c[2]);
      dc6 = - sinh(c[6]);
      dc10 = - sinh(c[10]);
      
      if( i%2 != 0 ){
        c2 = -c2;
        c6 = -c6;
        c10 = -c10;
        dc2 = -dc2;
        dc6 = -dc6;
        dc10 = -dc10;
      }
      
      switch(a){
        case 0:
          df = exp(tt * c[1]) * ( 
                 1.0 + c2 * exp(tt * c[3]) 
                 + c[4] * exp(tt * c[5]) * ( 
                   1.0 + c6 * exp(tt * c[7]) 
                   + c[8] * exp(tt * c[9]) * ( 
                     1.0 + c10 * exp(tt * c[11]) 
                     )
                   )
                 )
               + exp(tsym * c[1]) * ( 
                 1.0 + c2 * exp(tsym * c[3]) 
                 + c[4] * exp(tsym * c[5]) * ( 
                   1.0 + c6 * exp(tsym * c[7]) 
                   + c[8] * exp(tsym * c[9]) * ( 
                     1.0 + c10 * exp(tsym * c[11]) 
                     ) 
                   ) 
                 );
          break;

        case 1:
          df = c[0] * ( 
                tt * exp(tt * c[1]) * ( 
                  1.0 + c2 * exp(tt * c[3]) 
                  + c[4] * exp(tt * c[5]) * ( 
                    1.0 + c6 * exp(tt * c[7]) 
                    + c[8] * exp(tt * c[9]) * ( 
                      1.0 + c10 * exp(tt * c[11]) 
                      )
                    )
                   )
                + tsym * exp(tsym * c[1]) * ( 
                  1.0 + c2 * exp(tsym * c[3]) 
                  + c[4] * exp(tsym * c[5]) * ( 
                    1.0 + c6 * exp(tsym * c[7]) 
                    + c[8] * exp(tsym * c[9]) * ( 
                      1.0 + c10 * exp(tsym * c[11]) 
                      ) 
                    ) 
                   )
                );
          break;

        case 2:
          df = c[0] * dc2 * 
              ( exp(tt * (c[1]+c[3])) + exp(tsym * (c[1]+c[3])) );
          break;

        case 3:
          df = c[0] * c2 *
              ( exp(tt * (c[1]+c[3])) * tt + exp(tsym * (c[1]+c[3])) * tsym );
          break;
        
        case 4:
          df = c[0] * ( 
                 exp(tt * (c[1]+c[5])) * ( 
                   1.0 + c6 * exp(tt * c[7]) 
                   + c[8] * exp(tt * c[9]) * ( 
                     1.0 + c10 * exp(tt * c[11]) 
                     )
                   )
                 + exp(tsym * (c[1]+c[5])) * ( 
                   1.0 + c6 * exp(tsym * c[7]) 
                   + c[8] * exp(tsym * c[9]) * ( 
                     1.0 + c10 * exp(tsym * c[11]) 
                     ) 
                   ) 
               );
          break;

        case 5:
          df = c[0] * c[4] * ( 
                 exp(tt * (c[1]+c[5])) * tt * (
                   1.0 + c6 * exp(tt * c[7])
                   + c[8] * exp(tt * c[9]) * ( 
                     1.0 + c10 * exp(tt * c[11]) 
                     )
                   )
                 + exp(tsym * (c[1]+c[5])) * tsym * (
                   1.0 + c6 * exp(tsym * c[7])
                   + c[8] * exp(tsym * c[9]) * ( 
                     1.0 + c10 * exp(tsym * c[11]) 
                     ) 
                   ) 
              );
          break;

        case 6:
          df = c[0] * c[4] * dc6 *
              ( exp(tt * (c[1]+c[5]+c[7])) + exp(tsym * (c[1]+c[5]+c[7])) );
          break;

        case 7:
          df = c[0] * c[4] * c6 *
              ( exp(tt * (c[1]+c[5]+c[7])) * tt + exp(tsym * (c[1]+c[5]+c[7]) ) * tsym );
          break;

        case 8:
          df = c[0] * c[4] * (
                exp(tt * (c[1]+c[5]+c[9])) * ( 1.0 + c10 * exp(tt * c[11]) )
                 + exp(tsym * (c[1]+c[5]+c[9])) * ( 1.0 + c10 * exp(tsym * c[11]) )
                );
          break;
        
        case 9:
          df = c[0] * c[4] * c[8] * (
                exp(tt * (c[1]+c[5]+c[9])) * tt * ( 1.0 + c10 * exp(tt * c[11]) )
                 + exp(tsym * (c[1]+c[5]+c[9])) * tsym * ( 1.0 + c10 * exp(tsym * c[11]) )
                );
          break;
        
        case 10:
          df = c[0] * c[4] * c[8] * dc10 * (
                exp(tt * (c[1]+c[5]+c[9]+c[11])) 
                + exp(tsym * (c[1]+c[5]+c[9]+c[11])) 
                );
          break;
        
        case 11:
          df = c[0] * c[4] * c[8] * c10 * (
                exp(tt * (c[1]+c[5]+c[9]+c[11])) * tt
                + exp(tsym * (c[1]+c[5]+c[9]+c[11])) * tsym
                );
          break;
      }
    }
    else{
      exit(100);
    }

    return df;
}


double chiSq(const gsl_vector *params, void *fit){

  struct FitData *f = (struct FitData *)fit;
  
  const double *c = gsl_vector_const_ptr(params,0);
  const double *p = gsl_vector_const_ptr(f->p,0);
  const double *iw = gsl_vector_const_ptr(f->iw,0);

  int fr, to, end, npa, npt;
  double chisq;
  double dtmp;
  
  fr = f->n1;
  to = f->n2;
  end = f->nt;
  gsl_matrix *ic = f->invcov;
  npa = f->nparam;
  
  npt = to-fr+1;
  
  gsl_vector *delta = gsl_vector_alloc(npt);
  
  for (int i=0; i<npt; i++) gsl_vector_set(delta,i,fitFunc(c,fr+i,end,npa));
  gsl_vector_sub(delta,f->avg);

  chisq = 0.0;

  // diagonal part
  for (int i=0; i<npt; i++) {
    dtmp = gsl_vector_get(delta,i);
    chisq += dtmp*gsl_matrix_get(ic,i,i)*dtmp;
  }

  // offdiagonal part: use the symmetric property of a(n) (inverse) covariance matrix.
  for (int i=0; i<npt-1; i++) {
    dtmp = 0.0;
    for (int j=i+1; j<npt; j++) {
     dtmp += gsl_matrix_get(ic,i,j)*gsl_vector_get(delta,j);
    }
     chisq += 2.0*gsl_vector_get(delta,i)*dtmp;
  }

  // prior part
  for (int i=0; i<npa; i++) {
    dtmp = iw[i]*(c[i]-p[i]);
    chisq += dtmp*dtmp;
  }

  gsl_vector_free(delta);

  return chisq;
}


void dchiSq(const gsl_vector *params, void *fit, gsl_vector *dchisq)
{
  struct FitData *f = (struct FitData *)fit;
  
  const double *c = gsl_vector_const_ptr(params,0);
  const double *p = gsl_vector_const_ptr(f->p,0);
  const double *iw = gsl_vector_const_ptr(f->iw,0);

  int fr, to, end, npa, npt;
  double elem;
  double dtmp;
  
  fr = f->n1;
  to = f->n2;
  end = f->nt;
  gsl_matrix *ic = f->invcov;
  npa = f->nparam;
  
  npt = to-fr+1;
  
  gsl_vector *delta = gsl_vector_alloc(npt);
  
  for (int i=0; i<npt; i++) gsl_vector_set(delta,i,fitFunc(c,fr+i,end,npa));
  gsl_vector_sub(delta,f->avg);

  for (int a=0; a<npa; a++) {
    elem = 0.0;

    // diagonal part
    for (int i=0; i<npt; i++) {
      elem += gsl_vector_get(delta,i)*gsl_matrix_get(ic,i,i)*da_dfitFunc(c,a,fr+i,end,npa);
    }

    // offdiagonal part: use the symmetric property of a(n) (inverse) covariance matrix.
    for (int i=0; i<npt-1; i++) {
      dtmp = 0.0;
      for (int j=i+1; j<npt; j++) {
       dtmp += gsl_matrix_get(ic,i,j)*da_dfitFunc(c,a,fr+j,end,npa);
      }
      elem += 2.0*gsl_vector_get(delta,i)*dtmp;
    }
    
    // prior part
    elem += iw[a]*iw[a]*(c[a]-p[a]);

    gsl_vector_set(dchisq,a,2.0*elem);
  }
  
  gsl_vector_free(delta);

}

void chiSq_dchiSq(const gsl_vector *params, void *fit, double *chisq, gsl_vector *dchisq){

  *chisq = chiSq(params,fit);
  dchiSq(params,fit,dchisq);
}
